/*
File: Exploration.cpp
---------------------------
For details see Mutations.h
*/

#include "stdafx.h"
#include "Exploration.h"

using namespace std;

void EA::Supplement(vector<uint> & chromosome,
                    default_random_engine & engine)
{
    iota(chromosome.begin(), chromosome.end(), 0);
    shuffle(chromosome.begin(), chromosome.end(), engine);
}