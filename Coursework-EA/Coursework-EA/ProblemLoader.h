/*
File: ProblemLoader.h
---------------------------------------------------------
Details: A object used for parsing test case txt files
into a collection of jobs and a grid of times. The class
provides accessor fuctions to use the loaded information.

Date:               March 2015
Auther:     Andrius Jefremovas
Desc: Parses test cases from txt problem files.
---------------------------------------------------------
*/

#ifndef PROBLEM_LOADER_H
#define PROBLEM_LOADER_H

namespace EA
{
    // Structure of a single Job
    typedef struct
    {
        uint id;
        uint pickup;
        uint setDown;
        uint available;
        uint payments[4][2];
    } Job;

    // Class that loads jobs to form a problem
    class ProblemLoader
    {
    public:
        // The first construction will load a file
        ProblemLoader(const std::string & filename);
        ~ProblemLoader();

        // Loads specified problem
        void LoadProblem(const std::string & filename);
        // Resets current problem
        void ResetProblem();

        // Return a reference to a loaded collection of times and jobs
        const std::vector<Job> & GetJobs() const { return m_jobs; }
        const uint & GetTime(const uint row, const uint col) { return m_times[row][col]; }

    private:
        // A collection of jobs and grid times
        std::vector<Job> m_jobs;
        std::vector<std::vector<uint>> m_times;
    };
}

#endif