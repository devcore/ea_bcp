/*
File: Selection.cpp
---------------------------
For details see Selection.h
*/

#include "stdafx.h"
#include "Selection.h"

using namespace std;

uint EA::TournamentBest(const uint tournamentSize,
                        const vector<uint> & fitness,
                        default_random_engine & e,
                        const uniform_int_distribution<uint> & dist)
{
    uint picked{ 0 }, bestIndx{ 0 }, bestFit{ 0 };

    // Assume a random one is the best
    bestIndx = dist(e);
    bestFit = fitness[picked];
    
    // Try a sample and return the best
    for (uint i = 0; i < tournamentSize; ++i)
    {
        picked = dist(e);
        if (fitness[picked] > bestFit)
        {
            bestFit = fitness[picked];
            bestIndx = picked;
        }
    }
    return bestIndx;
}/* TournamentBest END {...} */

uint EA::TournamentWorst(const uint tournamentSize,
                         const vector<uint> & fitness,
                         default_random_engine & e,
                         const uniform_int_distribution<uint> & dist)
{
    uint picked{ 0 }, worstIndx{ 0 }, worstFit{ 0 };

    // Assume a random one is the worst
    worstIndx = dist(e);
    worstFit = fitness[worstIndx];

    // Try a sample and return the worst
    for (uint i = 0; i < tournamentSize; i++)
    {
        picked = dist(e);
        if (fitness[picked] < worstFit)
        {
            // Current worst
            worstFit = fitness[picked];
            worstIndx = picked;
        }
    }
    return worstIndx;
}/* TournamentWorst END {...} */