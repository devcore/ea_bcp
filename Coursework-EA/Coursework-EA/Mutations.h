/*
File: Mutations.h
---------------------------------------------------------
Details: Contains algorithms to perform mutations for the
evolutionary algorithm with some techniques inspired from:
A.E Ebiben and J.E. Smith, "Introduction to Evolutionary
Computing, Genetic Algorithms". They are modified and are
mainly programed to mutate ordered solutions - permutations.

Date:               March 2015
Auther:     Andrius Jefremovas
Desc: Holds functions for performing mutations.
---------------------------------------------------------
*/

#ifndef MUTATION_H
#define MUTATION_H

namespace EA
{
    // Selects 2 points and moves the latter one to the
    // first one, thus preserving order and adjacency.
    void InsertMutation(std::vector<uint> & chr,
                        std::default_random_engine & e,
                        const std::uniform_int_distribution<uint> & dist);

    // Picks 2 points and swaps their places, thus it
    // preservers adjacency, however breaks 4 links.
    void SwapMutation(std::vector<uint> & chr,
                      std::default_random_engine & e,
                      const std::uniform_int_distribution<uint> & dist);

    // Selects 2 points and inverts points between, thus
    // preserving adjacency with only 2 broken links.
    void InvertMutation(std::vector<uint> & chr,
                        std::default_random_engine & e,
                        const std::uniform_int_distribution<uint> & dist);

    // Pick 2 points and shuffle points in between, thus
    // potentially only breaking 2 links, however majorly
    // affecting the mutate points.
    void ShuffleMutation(std::vector<uint> & chr,
                         std::default_random_engine & e,
                         const std::uniform_int_distribution<uint> & dist);

    // Generate 2 random numbers, which are not equal to each other.
    void RandomPointsMut(uint & p1, uint & p2,
                         std::default_random_engine & e,
                         const std::uniform_int_distribution<uint> & dist);
}

#endif