/*
File: EvolAlgorithm.h
---------------------------------------------------------
Details: Contains an implementation of an evolutionary
algorithm, which uses various mutations and a couple of
cross ever functions to mutate a population of solutions,
that are represented as permutations.

Date:               March 2015
Auther:     Andrius Jefremovas
Desc: Holds functions for performing mutations.
---------------------------------------------------------
*/

#ifndef EVOL_ALG_H
#define EVOL_ALG_H

#include "ProblemLoader.h"

namespace EA
{
    class EvolAlg
    {
    public:
        /* Creates an Evolutionary Algorithm. Requires: number of iterations,
        tournament size, population size, seed, mutation chance and filepath*/
        EvolAlg(const uint iterations,
                const uint tournSize,
                const uint popSize,
                const uint seed,
                const std::string & filepath);

        // Default destructor
        ~EvolAlg();

        // Initialise a population of random solutions and evaluate them
        void SetupPopulation(const uint seed);

        /* Signature:
           CrossChance - the chance of crossover occuring (0.0 to 1.0).
           MutChance - the chance of mutation occuring (0.0 to 1.0).
           ReplaceCross - percentage of population to crossover (0.0 to 0.5).
           ReplaceMutate - percentage of pupulation to mutate (0.0 to 0.5).
           Explore - percentage of pupulation to renew (0.0 to 0.5).*/
        void SetupSteadyStateGen(const float crossChance,
                                 const float mutChance,
                                 const float replaceCross,
                                 const float replaceMut,
                                 const float explore);

        /*  Lauches a modified steady-state generational model iteration, 
            where a percentage of the population is renewed every 10th 
            iteration, a percentage of parents are replaced by offsprings
            every iteration and a percentage of the population is mutated
            vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
            Signature: crossover function and mutation function*/
        void SteadyStateGen(const std::function<void(const std::vector<uint> & p1,
                                                     const std::vector<uint> & p2,
                                                     std::vector<uint> & c1, 
                                                     std::vector<uint> & c2,
                                                     std::default_random_engine & e,
                                                     const std::uniform_int_distribution<uint> & dist
                                                    )> & crossover,
                            const std::function<void(std::vector<uint> & chr,
                                                     std::default_random_engine & e,
                                                     const std::uniform_int_distribution<uint> & dist
                                                    )> & mutation);

        // Finds the fittest chromosome
        uint GetFittest();

    private:
        // Select a default test case and load it
        void InitialiseProblem(const std::string & filepath);

        // Set parameters that correspond with the problem
        void InitialiseEA(const uint iterations,
                          const uint tournSize,
                          const uint popSize);


        // Outputs the best chromosome to the console and return the index
        uint PrintBestChr();

        // Evaluates fitness of solution
        uint EvaluateFitness(const std::vector<uint> & chromosome);

        uint m_jobSize;
        uint m_tournSize;	  					        // Tournament size
        uint m_iterations;	  					        // Number of iterations
        uint m_populationSize;					        // Size of population
        std::vector<uint> m_fitness;			        // Fitness array
        std::vector<std::vector<uint>> m_population;    // Population array

        float m_crossChance;                            // Chance for crossover to occur
        float m_mutChance;                              // Chance for mutation to occur
        float m_replaceCross;                           // Percentage to crossover
        float m_replaceMut;                             // Percentage to mutate
        float m_explore;                                // Percentage of the pupulation to renew

        std::unique_ptr<ProblemLoader> m_problem;       // Load problem
        std::default_random_engine m_engine;            // Random Engine
    };
}

#endif