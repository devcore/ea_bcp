/*
File: ProblemLoader.cpp
---------------------------
For details see ProblemLoader.h
*/

#include "stdafx.h"
#include "ProblemLoader.h"
#include <string>

using namespace std;
using namespace EA;

// Offsets for parsing file
const uint offset{ 2 }, jobSet{ 12 };

ProblemLoader::ProblemLoader(const std::string & filename)
{
    LoadProblem(filename);
}

ProblemLoader::~ProblemLoader()
{
    m_jobs.clear();
    m_times.clear();

    cerr << "LOG >> Problem loader destroyed" << endl;
}


void ProblemLoader::LoadProblem(const std::string & filename)
{
    ResetProblem(); // Make sure the collections are empty
    system("cls");  // Clear console

    string line;				// Read string to store in vector
    vector<string> lineVec;		// Vector of strings for parsing
    ifstream file(filename);	// Openfile to read
    
    // Parses data from file into separate values and stores it
    while (getline(file, line))
    {
        size_t prev{ 0 }, pos{ 0 };
        while ((pos = line.find_first_of(" ','\n'\t", prev)) != std::string::npos)
        {
            if (pos > prev)
                lineVec.push_back(line.substr(prev, pos - prev));
            prev = pos + 1;
        }
        if (prev < line.length())
            lineVec.push_back(line.substr(prev, std::string::npos));
    }
    file.close();
    cerr << "LOG >> File loaded" << endl;

    {/*** Time Grid ***/

        // Finds location the grid of times
        uint timePos = distance(lineVec.begin(), find(lineVec.begin(), lineVec.end(), "times"));
        uint times = stoul(lineVec[timePos + 1]);
        m_times.resize(times); // Resize collection to match row of times

        // Store times in a vector grid
        uint i{ 0 }, j{ 0 };
        for (uint n = timePos + offset; n < times*times + timePos + offset; n++)
        {
            // Add element to time grid
            m_times[i].push_back(stoul(lineVec[n]));
            // Increment Row and Column
            ++i; if (i == times) { ++j; i = 0; }
        }
        cerr << "LOG >> Times Parsed" << endl;

    }/*** Time Grid End ***/

    {/*** Job Parse ***/

        // Finds job grid location
        uint jobPos = distance(lineVec.begin(), find(lineVec.begin(), lineVec.end(), "jobs"));
        uint jobs = stoul(lineVec[jobPos + 1]);
        m_jobs.resize(jobs);

        // Store jobs
        uint i{ 0 };
        for (uint n = jobPos + offset; n < jobs*jobSet + jobPos + offset; n++)
        {
            // Parse Job
            m_jobs[i].id = stoul(lineVec[n]);
            m_jobs[i].pickup = stoul(lineVec[n + 1]);
            m_jobs[i].setDown = stoul(lineVec[n + 2]);
            m_jobs[i].available = stoul(lineVec[n + 3]);
            // Parse Payments
            m_jobs[i].payments[0][0] = stoul(lineVec[n + 4]);
            m_jobs[i].payments[0][1] = stoul(lineVec[n + 5]);
            m_jobs[i].payments[1][0] = stoul(lineVec[n + 6]);
            m_jobs[i].payments[1][1] = stoul(lineVec[n + 7]);
            m_jobs[i].payments[2][0] = stoul(lineVec[n + 8]);
            m_jobs[i].payments[2][1] = stoul(lineVec[n + 9]);
            m_jobs[i].payments[3][0] = stoul(lineVec[n + 10]);
            m_jobs[i].payments[3][1] = stoul(lineVec[n + 11]);
            // Increment job set and problem
            n += jobSet-1; ++i;
        }
        cerr << "LOG >> Jobs Parsed" << endl;

    }/*** Job Parse End ***/

    lineVec.clear();
}

void ProblemLoader::ResetProblem()
{
    m_jobs.clear();
    m_times.clear();
    cerr << "LOG >> Problem reset" << endl << endl;
}