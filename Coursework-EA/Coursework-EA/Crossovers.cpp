/*
File: Crossovers.cpp
---------------------------
For details see Crossovers.h
*/

#include "stdafx.h"
#include "Crossovers.h"

using namespace std;

void EA::OrderPntCrossover(const vector<uint> & parent1,
                           const vector<uint> & parent2,
                           vector<uint> & child1,
                           vector<uint> & child2,
                           default_random_engine & e,
                           const uniform_int_distribution<uint> & dist)
{
    // Generate temporary vector and 2 random points
    uint n{ 0 }, p1{ 0 }, p2{ 0 }, size{ parent1.size() };
    RandomPointsCross(p1, p2, e, dist);
    vector<uint> temp(size  - (p2 - p1));

    // Store the unique points of the second parent in order
    for (uint i = 0; i < size; ++i)
    {
        if (!(find(parent1.begin() + p1, parent1.begin() + p2, parent2[i]) != (parent1.begin() + p2)))
        {
            temp[n] = parent2[i];
            ++n;
        }
    }
    // Copy from p1 to p2 from parent1
    std::copy(parent1.begin() + p1, parent1.begin() + p2, child1.begin() + p1);
    // Copy first part of temporary vector
    std::copy(temp.begin(), temp.begin() + p1, child1.begin());
    // Copy second part of temporary vector
    std::copy(temp.begin() + p1, temp.end(), child1.begin() + p2);

    n = 0; // Reset counter
    // Store the unique points of the second parent in order
    for (uint i = 0; i < size; ++i)
    {
        if (!(find(parent2.begin() + p1, parent2.begin() + p2, parent1[i]) != (parent2.begin() + p2)))
        {
            temp[n] = parent1[i];
            ++n;
        }
    }
    // Copy from p1 to p2 from parent2
    std::copy(parent2.begin() + p1, parent2.begin() + p2, child2.begin() + p1);
    // Copy first part of temporary vector
    std::copy(temp.begin(), temp.begin() + p1, child2.begin());
    // Copy second part of temporary vector
    std::copy(temp.begin() + p1, temp.end(), child2.begin() + p2);
}/* OrderPntCrossover END {...} */

void EA::PartMapCrossover(const vector<uint> & parent1,
                          const vector<uint> & parent2,
                          vector<uint> & child1,
                          vector<uint> & child2,
                          default_random_engine & e,
                          const uniform_int_distribution<uint> & dist)
{
    // Generate temporary vector and 2 random points
    uint n{ 0 }, p1{ 0 }, p2{ 0 }, size{ parent1.size() };
    RandomPointsCross(p1, p2, e, dist);

    // Copy part of parent 1 to child 1
    std::copy(parent1.begin() + p1, parent1.begin() + p2, child1.begin() + p1);
    // Map parent 2 to child 1
    for (uint i = p1; i < p2; ++i)
    {
        if (!(find(parent1.begin() + p1, parent1.begin() + p2, parent2[i]) != (parent1.begin() + p2)))
        {
            uint val = find(parent2.begin(), parent2.end(), parent1[i]) - parent2.begin();
            if (val < p1 || p2 > val)
                child1[val] = parent2[i];
        }
    }
    // Fill what is left in parent 2 to child 1
    for (uint i = 0; i < size; ++i)
    {
        for (uint j = 0; j < size; ++j)
        {
            if (!(find(child1.begin(), child1.end(), parent2[j]) != child1.end()))
                child1[i] = parent2[j];
        }
    }

    // Copy part of parent 2 to child 2
    std::copy(parent2.begin() + p1, parent2.begin() + p2, child2.begin() + p1);
    // Map parent 1 to child 2
    for (uint i = p1; i < p2; ++i)
    {
        if (!(find(parent2.begin() + p1, parent2.begin() + p2, parent1[i]) != (parent2.begin() + p2)))
        {
            uint val = find(parent1.begin(), parent1.end(), parent2[i]) - parent1.begin();
            if (val < p1 || p2 > val)
                child2[val] = parent1[i];
        }
    }
    // Fill what is left in parent 2 to child 1
    for (uint i = 0; i < size; ++i)
    {
        for (uint j = 0; j < size; ++j)
        {
            if (!(find(child2.begin(), child2.end(), parent1[j]) != child2.end()))
                child2[i] = parent1[j];
        }
    }
}/* PartMapCrossover END {...} */

void EA::RandomPointsCross(uint & p1, uint & p2,
                           default_random_engine & e,
                           const uniform_int_distribution<uint> & dist)
{
    while (true)
    {
        // Pick a random numbers
        p1 = dist(e);
        p2 = dist(e);
        // If p1 is greater than p2 swap them.
        if (p2 < p1)
            swap(p1, p2);
        if (p1 != p2)
            break;
    }
}/* RandomPointsCross END {...} */