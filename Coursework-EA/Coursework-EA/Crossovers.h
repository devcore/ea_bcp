/*
File: Crossovers.h
---------------------------------------------------------
Details: Contains algorithms to perform crossovers for the
evolutionary algorithm with some techniques inspired from:
A.E Ebiben and J.E. Smith, "Introduction to Evolutionary
Computing, Genetic Algorithms". They are modified and are
mainly programed crossover ordered solutions - permutations.

Date:               March 2015
Auther:     Andrius Jefremovas
Desc: Holds functions for performing mutations.
---------------------------------------------------------
*/

#ifndef CROSS_OVERS_H
#define CROSS_OVERS_H

namespace EA
{
    // Order one crossover - preserves relative order.
    void OrderPntCrossover(const std::vector<uint> & p1,
                           const std::vector<uint> & p2,
                           std::vector<uint> & c1, 
                           std::vector<uint> & c2,
                           std::default_random_engine & e,
                           const std::uniform_int_distribution<uint> & dist);

    // Partially mapped crossover
    void PartMapCrossover(const std::vector<uint> & p1,
                          const std::vector<uint> & p2,
                          std::vector<uint> & c1,
                          std::vector<uint> & c2,
                          std::default_random_engine & e,
                          const std::uniform_int_distribution<uint> & dist);

    // Generate 2 random numbers, which are not equal to each other.
    void RandomPointsCross(uint & p1, uint & p2,
                           std::default_random_engine & e,
                           const std::uniform_int_distribution<uint> & dist);
}

#endif