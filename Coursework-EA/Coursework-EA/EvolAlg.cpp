/*
File: EvolAlg.cpp
---------------------------
For details see EvolAlg.h
*/
#include "stdafx.h"
#include "EvolAlg.h"
#include "Selection.h"
#include "Exploration.h"

using namespace std;
using namespace EA;

EvolAlg::EvolAlg(const uint iterations,
                 const uint tournSize,
                 const uint popSize,
                 const uint seed,
                 const std::string & filepath)
{
    // Prepare containers, settings.
    InitialiseProblem(filepath);
    InitialiseEA(iterations, tournSize, popSize);
    SetupPopulation(seed);
}/* Constructor END {...} */

EvolAlg::~EvolAlg()
{
    m_population.clear();
    m_fitness.clear();
    cerr << "LOG >> Evolutionary Algorithm destroyed" << endl;
}/* Destructor END {...} */

void EvolAlg::InitialiseProblem(const string & filepath)
{
    m_problem = make_unique<ProblemLoader>(filepath);
    m_jobSize = m_problem->GetJobs().size();
}/* InitialiseProblem END {...} */

void EvolAlg::InitialiseEA(const uint iterations,
                           const uint tournSize,
                           const uint popSize)
{
    // Set parameters
    m_iterations = iterations * m_jobSize;
    m_tournSize = tournSize;
    m_populationSize = popSize * m_jobSize;
    
    
    // Resize collections
    m_population.resize(m_populationSize);
    m_fitness.resize(m_populationSize);
}/* InitialiseEA END {...} */

void EvolAlg::SetupPopulation(const uint seed)
{
    // Set seed for random engine
    m_engine.seed(seed);
    for (uint i = 0; i < m_populationSize; i++)
    {
        // Resize vector to match number of jobs
        m_population[i].resize(m_jobSize);
        // Integrate each collection by index
        iota(m_population[i].begin(), m_population[i].end(), 0);
        // Shuffle the collections
        shuffle(m_population[i].begin(), m_population[i].end(), m_engine);
    }
    cerr << "LOG >> Population randomized" << endl;

    // Evaluate randomized population and record the best one
    uint bestRandom{ 0 };
    for (uint i = 0; i < m_populationSize; i++)
    {
        m_fitness[i] = EvaluateFitness(m_population[i]);
        if (m_fitness[i] > bestRandom)
            bestRandom = m_fitness[i];

    }
    cerr << "LOG >> Fitness of population evaluated" << endl;
    cerr << "LOG >> Best in generating population: " << m_fitness[PrintBestChr()] << endl;
}/* SetupPopulation END {...} */

void EvolAlg::SetupSteadyStateGen(const float crossChance,
                                  const float mutChance,
                                  const float replaceCross,
                                  const float replaceMut,
                                  const float explore)
{
    m_crossChance = crossChance;
    m_mutChance = mutChance;
    m_replaceCross = replaceCross;
    m_replaceMut = replaceMut;
    m_explore = explore;
}/* SetupSteadyStateGen END {...} */

void EvolAlg::SteadyStateGen(const std::function<void(const vector<uint> & p1,
                                                      const vector<uint> & p2,
                                                      vector<uint> & c1, 
                                                      vector<uint> & c2,
                                                      default_random_engine & e,
                                                      const uniform_int_distribution<uint> & dist
                                                     )> & crossover,
                             const std::function<void(vector<uint> & chr,
                                                      default_random_engine & e,
                                                      const uniform_int_distribution<uint> & dist
                                                     )> & mutation)
{
    // Create distributions for chance, jobs, population
    uniform_real_distribution<float> chance(0, 1);                   
    uniform_int_distribution<uint> distJob(0, m_jobSize-1);            
    uniform_int_distribution<uint> distPop(0, m_populationSize - 1); 
    
    cerr << "LOG >> Starting Steady State Generation" << endl;
    for (uint i = 0; i < m_iterations; ++i)
    {
        // Chance to crossover
        if (m_crossChance > chance(m_engine))
        {
            uint halfReplace{ static_cast<uint>(m_populationSize*m_replaceCross) };
            vector<uint> selections(halfReplace+halfReplace);

            // Select some good and bad chromosomes
            for (uint i = 0; i < halfReplace; i++)
            {
                selections[i] = TournamentBest(m_tournSize, m_fitness, m_engine, distPop);
                selections[i + halfReplace] = TournamentWorst(m_tournSize, m_fitness, m_engine, distPop);
            }

            // Crossover half of the good ones with half of the bad ones
            for (uint i = 0; i < halfReplace/2; i++)
                crossover(m_population[selections[i]], m_population[selections[i+1]],
                          m_population[selections[i+halfReplace]], m_population[selections[i+halfReplace+1]],
                          m_engine, distJob);
            
            // Evaluate their new fitness
            for (uint i = 0; i < halfReplace; i++)
                m_fitness[selections[i + halfReplace]] = EvaluateFitness(m_population[selections[i + halfReplace]]);
        } // Cross chance END

        // Chance to mutate
        if (m_mutChance > chance(m_engine))
        {
            uint replace{ static_cast<uint>(m_populationSize*m_replaceMut) };
            uint indx{ 0 };

            // Mutate population
            for (uint i = 0; i < replace; i++)
            {
                indx = TournamentWorst(m_tournSize, m_fitness, m_engine, distPop);
                mutation(m_population[indx], ref(m_engine), ref(distJob));
                m_fitness[indx] = EvaluateFitness(m_population[indx]);
            }
        }// Mut chance END

        // Print the best chromosome for each 10th iteration (a generation) and renew population
        if (i % (m_iterations / 10) == 0)
        {
            cerr << "LOG >> Generation: " << i / 10 << " << " << "Fittest: " << m_fitness[PrintBestChr()] << endl;
            //cerr << "LOG >> Discarding: " << m_explore * 100.0f << "%" << endl;
            uint replace{ static_cast<uint>(m_populationSize*m_explore) };
            uint indx{ 0 };

            // Renew part of the population
            for (uint i = 0; i < replace; i++)
            {
                indx = TournamentWorst(m_tournSize, m_fitness, m_engine, distPop);
                Supplement(m_population[indx], std::ref(m_engine));
                m_fitness[indx] = EvaluateFitness(m_population[indx]);
            }
        }
    }// For Generations END
    cerr << endl;
    cerr << "LOG >> <<Last Generation>> ";
    uint best = PrintBestChr();

    // Print the chromosome
    cerr << "Chromosome: ";
    for (uint i = 0; i < m_jobSize; i++)
       cerr << m_population[best][i] << " ";
    cerr << endl;
}/* SteadyStateGen END {...} */

uint EvolAlg::EvaluateFitness(const std::vector<uint> & chromosome)
{
    uint reward{ 0 }, location{ 0 }, time{ 0 };
    for (uint i = 0; i < m_jobSize; ++i)
    {
        Job job = m_problem->GetJobs()[chromosome[i]];
        time += m_problem->GetTime(location, job.pickup);

        if (time < job.available) 
            time = job.available;

        time += m_problem->GetTime(job.pickup, job.setDown);

        // Compute payment
        for (uint j = 0; j < 4; ++j)
        {
            if (time < job.payments[j][0])
            {
                reward += job.payments[j][1];
                break;
            }
        }
        location = job.setDown;
    }
    return reward;
}/* EvaluateFitness END {...} */

uint EvolAlg::PrintBestChr()
{
    // Find the index of the fittest
    uint best = distance(m_fitness.begin(), max_element(m_fitness.begin(), m_fitness.end()));
    /*cerr << " Chromosome: ";
    for (uint i = 0; i < m_jobSize; i++)
        cerr << m_population[best][i] << " ";
    cerr << endl;*/
    return best;
}/* PrintBestChr END {...} */

uint EvolAlg::GetFittest()
{
    return *max_element(m_fitness.begin(), m_fitness.end());
}/* GetFittest END {...} */