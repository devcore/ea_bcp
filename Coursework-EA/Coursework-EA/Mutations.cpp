/*
File: Mutations.cpp
---------------------------
For details see Mutations.h
*/

#include "stdafx.h"
#include "Mutations.h"

using namespace std;

void EA::InsertMutation(vector<uint> & chr,
                        default_random_engine & e,
                        const uniform_int_distribution<uint> & dist)
{
    // Get two random points
    uint p1{ 0 }, p2{ 0 }, size{chr.size()};
    RandomPointsMut(p1, p2, e, dist);

    vector<uint> temp = chr;
    chr[p1 + 1] = chr[p2];

    // Copy the part between and insert it
    std::copy(temp.begin() + p1 + 1, temp.begin() + p2, chr.begin() + p1 + 2);
}/* InsertMutation END {...} */

void EA::SwapMutation(vector<uint> & chr,
                      default_random_engine & e,
                      const uniform_int_distribution<uint> & dist)
{
    // Get two random points
    uint p1{ 0 }, p2{ 0 };
    RandomPointsMut(p1, p2, e, dist);

    // Swap 2 iterator values
    std::iter_swap(chr.begin() + p1, chr.begin() + p2);
}/* SwapMutation END {...} */

void EA::InvertMutation(vector<uint> & chr,
                        default_random_engine & e,
                        const uniform_int_distribution<uint> & dist)
{
    // Get two random points
    uint p1{ 0 }, p2{ 0 };
    RandomPointsMut(p1, p2, e, dist);

    // Inverse part between the two points
    std::reverse(chr.begin() + p1, chr.begin() + p2);
}/* InvertMutation END {...} */

void EA::ShuffleMutation(vector<uint> & chr,
                         default_random_engine & e,
                         const uniform_int_distribution<uint> & dist)
{
    // Get two random points
    uint p1{ 0 }, p2{ 0 };
    RandomPointsMut(p1, p2, e, dist);

    // Temporary storage for sub-part
    vector<uint> tempVec;
    tempVec.resize(p2 - p1);

    // Store sub-part of chromosome
    for (uint i = p1; i < p2; ++i)
        tempVec[i - p1] = chr[i];

    // Shuffle the temp vector
    shuffle(tempVec.begin(), tempVec.end(), e);

    // Add the sub-part back to the chromosome
    for (uint i = p1; i < p2; ++i)
        chr[i] = tempVec[i - p1];

}/* ShuffleMutation END {...} */

void EA::RandomPointsMut(uint & p1, uint & p2, 
                         default_random_engine & e,
                         const uniform_int_distribution<uint> & dist)
{
    while (true)
    {
        // Pick a random numbers
        p1 = dist(e);
        p2 = dist(e);
        // If p1 is greater than p2 swap them.
        if (p2 < p1)
            swap(p1, p2);
        if (p1 != p2)
            break;
    }
}/* RandomPointsMut END {...} */