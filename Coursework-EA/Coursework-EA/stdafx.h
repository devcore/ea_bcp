/*----------------------------------*/
/*-General includes and definitions-*/
/*----------------------------------*/

#ifndef INC_H
#define INC_H

// Generic includes
#include <memory>
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>
#include <string>
#include <thread>
#include <random>
#include <functional>
#include <algorithm>
#include <numeric>
#include <array>

// Abbriviation for unsigned integers
typedef unsigned int uint;

#endif