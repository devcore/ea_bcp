/*
File: Selection.h
---------------------------------------------------------
Details: Contains algorithms to perform selection for the
evolutionary algorithm e.g. Tournament.

Date:               March 2015
Auther:     Andrius Jefremovas
Desc: Holds functions for performing mutations.
---------------------------------------------------------
*/

#ifndef SELECTION_H
#define SELECTION_H

namespace EA
{
    /* Checks a random n amount of the population
    and return the best from the sample.*/
    uint TournamentBest(const uint tournamentSize,
                        const std::vector<uint> & fitness,
                        std::default_random_engine & e,
                        const std::uniform_int_distribution<uint> & dist);

    /* Checks a random n amount of the population
    and return the worst from the sample.*/
    uint TournamentWorst(const uint tournamentSize,
                         const std::vector<uint> & fitness,
                         std::default_random_engine & e,
                         const std::uniform_int_distribution<uint> & dist);
}

#endif