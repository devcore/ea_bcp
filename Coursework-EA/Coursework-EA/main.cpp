/*
File: main.cpp
---------------------------------------------------------
Details: This file contains the constructions, setup and 
the start of the evolutionary algorithm. It is also used
to perfrom tests and store them in files.
---------------------------------------------------------
Date:               March 2015
Auther:     Andrius Jefremovas
Desc:  Application entry point
---------------------------------------------------------
*/
#include "stdafx.h"
#include "EvolAlg.h"
#include "Crossovers.h"
#include "Mutations.h"

// Perform Genetic algorithm repeatedly to get average
#define AVERAGE_TEST 0
 // Known best results (must corespond with file number i.e. ..\\Cases\\Problem1.txt)
const uint Test1{ 330 }, Test2{ 407 }, Test3{ 719 }, Test4{ 996 }, Test5{ 958 };

void main()
{
    uint iter   { 120 }, // Number of generations 
         tourn  { 3   }, // Tournament Size       
         pop    { 200 }, // Population Size       
         seed   { static_cast<uint>(time(0)) }; // Seed !!!
    // Create Evolutionary Algorithm test case
    EA::EvolAlg ea{ iter, tourn, pop, seed, "..\\Cases\\Problem2.txt" };
    float crossChc  { 0.2f }, // Crossover Chance       || Range 0.0 .. 1.0      
          mutChc    { 0.2f }, // Mutation Chance        || Range 0.0 .. 1.0   
          crossPct  { 0.1f }, // Cross percentage       || Range 0.0 .. 0.5   
          mutPct    { 0.1f }, // Mutation percentage    || Range 0.0 .. 0.5   
          explore   { 0.3f }; // Exploration percentage || Range 0.0 .. 1.0   
    // Setup crossover and mutation parameters
    ea.SetupSteadyStateGen(crossChc, mutChc, crossPct, mutPct, explore);
    /* Launch steady state generation with one of each of these functions
    Crossovers:                  Mutations:
        EA::OrderPntCrossover       EA::InsertMutation
        EA::PartMapCrossover        EA::SwapMutation
                                    EA::InvertMutation
                                    EA::ShuffleMutation
    */

    // Create random engine to use throught the program
    std::default_random_engine e;
    std::uniform_int_distribution<uint> dist;
#if AVERAGE_TEST // Perform Genetic algorithm repeatedly to get average
    uint avgFitness{ 0 }, iterations{ 10 };
    for (uint i = 0; i < iterations; i++)
    {
        ea.SteadyStateGen(EA::OrderPntCrossover, EA::SwapMutation);
        avgFitness += ea.GetFittest();
        ea.SetupPopulation(dist(e));
    }
    std::cerr << "Average: " << (float)avgFitness / (Test1*iterations) << "\n";
#else // Perform genetic algorithm once to get result
    ea.SteadyStateGen(EA::OrderPntCrossover, EA::SwapMutation);
    std::cerr << "Final Fitness: " << (float)ea.GetFittest()/Test2*100 << "%" << std::endl;
#endif
    std::system("Pause");
}