/*
File: Exploration.h
---------------------------------------------------------
Details: Contains algorithms to perform exploration for
the evolutionary algorithm e.g. Replacing a percentage
of bad chromosome with newly generated ones.

Date:               March 2015
Auther:     Andrius Jefremovas
Desc: Holds functions for performing mutations.
---------------------------------------------------------
*/

#ifndef EXPLORATION_H
#define EXPLORATION_H

namespace EA
{
    /* Supplements the population by replacing a portion
    of the population by new genomes.*/
    void Supplement(std::vector<uint> & chromosome,
                    std::default_random_engine & engine);
}

#endif