## **REQUIREMENTS** ##
Microsoft Visual Studio 2012 or newer.
## **SUMMARY** ##
An implementation of a [Genetic Algorithm](https://en.wikipedia.org/wiki/Genetic_algorithm) (also known as an Evolutionary algorithm). Features 2 crossover, 4 mutation operators and a steady-state generation system.
### EXAMPLE USAGE (similar to main.cpp) ###
```
#!c++
    // Create Evolutionary Algorithm with test case
    EA::EvolAlg ea{ iterations,      // Number of generations 
                    tournament_size, // Size of tournament to select good/bad chromosomes
                    population_size, // Number of chromosomes generated
                    seed, // Seed for random engine
                    "..\\Cases\\Problem1.txt" };
    // Setup crossover, mutation and exploration parameters
    ea.SetupSteadyStateGen(crossover_chance, // chance that a crossover occurs
                           mutation_chance, // chance that a mutation occurs
                           crossover_percentage, //  Percentage of population to crossover
                           mutation_percentage, //  Percentage of population to mutate
                           explore_percentage); //  Percentage of population to recreate
    // Create random engine to use throught the program
    std::default_random_engine e;
    std::uniform_int_distribution<uint> dist;
    // Setup population using the random engine
    ea.SetupPopulation(dist(e));
    // Launch genetic algorithm with one of the crossover and mutation fuctions
    ea.SteadyStateGen(EA::OrderPntCrossover, EA::SwapMutation);
    // To get how close to a perfect answer with the default test cases
    std::cerr << "Final Fitness: " << (float)ea.GetFittest()/Test1*100 << "%" << std::endl;
```
### **NOTICE** ###
Copyright 2015 Andrius Jefremovas

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.